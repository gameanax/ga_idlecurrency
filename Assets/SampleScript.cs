﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SampleScript : MonoBehaviour
{
    public IdleCurrency Value1;
    public IdleCurrency Value2;

    void Start()
    {

        //IdleCurrency a = new IdleCurrency(8, 22);
        //IdleCurrency b = new IdleCurrency(5, 20);
        //IdleCurrency c = b * a;
        //Debug.Log(c.Value + "   " + c.Exp);

        //Debug.Log(c.ToShortString());

    }

    List<string> CurrencyMileStonesAchieved {
        get {
            List<string> temp = new List<string>();
            string str = PlayerPrefs.GetString("CurrencyMileStonesAchieved", "");
            if (string.IsNullOrEmpty(str))
            {
                return temp;
            }
            else {
                string[] strArr = str.Split(',');
                foreach (string s in strArr) {
                    temp.Add(s);
                }
                return temp;
            }
        }
        set {
            List<string> temp = value;
            string ToSave = "";
            foreach (string s in temp) {
                ToSave += (s + ",");
            }
            ToSave = ToSave.TrimEnd(',');
            ToSave = ToSave.Trim(',');
            PlayerPrefs.SetString("CurrencyMileStonesAchieved", ToSave);
        }
    }

    public void Add() {
        IdleCurrency newC = Value1 + Value2;
        Debug.Log("Result --> short(" + newC.ToShortString() + ")  long("+ newC.ToLongString() + ")  exponent(" + newC.ToExponentForm() + ")   SaveAbleString --> " + newC.GetStringForSave());
        Debug.Log("ShortString Suffix --> " + newC.GetShortStringSuffix());
    }

    public void Subtract()
    {
        IdleCurrency newC = Value1 - Value2;
        Debug.Log("Result --> short(" + newC.ToShortString() + ")  llong("+ newC.ToLongString() + ")  exponent(" + newC.ToExponentForm() + ")    SaveAbleString --> " + newC.GetStringForSave());

    }

    public void Multiply()
    {
        IdleCurrency newC = Value1 * Value2;
        Debug.Log("Result --> short(" + newC.ToShortString() + ")  llong("+ newC.ToLongString() + ")  exponent(" + newC.ToExponentForm() + ")    SaveAbleString --> " + newC.GetStringForSave());
    }
    public void Divide()
    {
        IdleCurrency newC = Value1 / Value2;
        Debug.Log("Result --> short(" + newC.ToShortString() + ")  llong("+ newC.ToLongString() + ")  exponent(" + newC.ToExponentForm() + ")    SaveAbleString --> " + newC.GetStringForSave());
    }

    public void Compare()
    {
        Debug.Log("Result --> " + (Value1==Value2));
    }
}
