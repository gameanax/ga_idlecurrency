Instructions for use

IdleCurrency.cs is the DataType to use

IdleCurrency exposes following Methods

1. ToShortString() // Use this method to get your formatted string for In-UI Display purpose
2. GetStringForSave() // This returns a string which can be used to save this data type in PlayerPrefs or other saving mechanisms
3. SetValueFromString(string s) //Pass the saved string in this method to Set the variables values from the saved data